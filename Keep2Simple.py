import os
import json
raiz = 'KeepJson/'    
salida ='SimplenotesJson/'
print('COMIENZO')
'''
Restructura archivos .JSON exportados de GoogleKeep para ser importados en Simplenotes.
'''
def simplenotes( ruta, salida ):
    ficheros = os.listdir( ruta )
    print( "> RECORRO: " + ruta  )
    for fichero in ficheros[::-1]:     
        if (
            fichero.endswith( '.json' ) or
            fichero.endswith( '.JSON' )
        ):
            print( 40 * 'v' )
            print( "@ PROCESO: " + ruta + fichero + "." ) 
            try:
                with open( ruta + fichero ) as json_file:
                    data = json.load( json_file )
                    este = {}
                    contexto  = data['textContent']
                    date      = data['userEditedTimestampUsec'] 
                    contenido = ''
                    for p in data['annotations']:                        
                        titulo      = p['title']
                        descripcion = p['description']  
                        url         = p['url'] 
                        tags        = [ 'fromKeep']
                        contenido   = titulo + '\n\n' + contexto + '\n\n' + descripcion + '\n\n' + url
                    este['content'] = contenido
                    este['tags'] = tags
                    este['savedate'] = date
                    este['creationDate'] = date
                    este['modificationDate'] = date
                    este['syncdate'] = date
                
                    print('contenido: ', contenido)
                    with open(salida + str(date) + '.json', 'w') as json_file:
                        json.dump(este, json_file)
            #except ValueError:
            #    print("Error: there was an error")
            except Exception as exc:
                print('!!!! ERROR !!!!')
                print( exc )
            #except:
            #    
                #pass    
            finally:
                print( 40 * '^' )
simplenotes( raiz, salida )
print(80 * '#')
print('FINALIZO')

