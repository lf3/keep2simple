# GoogleKeep to Simplenotes

Un script para el interprete Python con el fin de transformar la estructura que provee (exporta) [GoogleKeep](https://keep.google.com/) a la que acepta [Simplenotes](https://simplenote.com/) (importa).

Y una mini rutina en bash para subirlas desde la linea de comandados con [sncli](https://github.com/insanum/sncli).
